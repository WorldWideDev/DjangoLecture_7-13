# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib import messages
def some_silly_function():
    pass

MIN_LENGTH = 3

class UserManager(models.Manager):
    def check_it_out(self, request):
        data_to_check = request.POST

        cool = True

        # names must not be empty
        if len(data_to_check['first_name']) < 1 or len(data_to_check['last_name']) < 1 or len(data_to_check['username']) < 1:
            messages.error(request, "names must not be empty")
            cool = False
        if len(data_to_check['first_name']) < MIN_LENGTH or len(data_to_check['last_name']) < MIN_LENGTH:
            messages.error(request, "first/last names must must be greater than {}".format(MIN_LENGTH))
            cool = False
        # usernames must be unique
        if len(self.filter(username=data_to_check['username'])) > 0:
            messages.error(request, "usernames must be unique")
            cool = False
        # no empty spaces
        if cool:
            self.create(
                first_name = data_to_check['first_name'],
                last_name = data_to_check['last_name'],
                username = data_to_check['username']
            )
        return cool

# Create your models here.
class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    username = models.CharField(max_length=255, unique=True)
    objects = UserManager()