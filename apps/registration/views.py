# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import User, some_silly_function
from django.shortcuts import render, redirect, HttpResponse

# Create your views here.
def index(req):
    return render(req, 'registration/index.html')

def signup(request):
    if User.objects.check_it_out(request) == False:
        return redirect('/')
    return HttpResponse("YAY")